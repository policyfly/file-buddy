# File Buddy

File Buddy is a simple Flask App that can read and write to your file system so you can proxy
files into Postman requests with ease.

## Setup

File Buddy is rather simple to setup, just make sure you have some sort of virtual environment
manager installed and then:

```
$ virtualenv .venv
$ .venv/Scripts/activate
$(.venv) pip install -r requirements.txt
$(.venv) python file-buddy/app.py
```

You should be good to go!

## Packaging

To get File Buddy into a new Executable for a new release, the following command
should do it for you (make sure you've setup and activated the virtualenv first!):

```
$(.venv) cd file-buddy
$(.venv) pyinstaller.exe app.py
```

That should package file buddy up into app.exe in the dist folder for you for easy reuse.
