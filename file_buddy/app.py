from base64 import b64encode, b64decode
from urllib.parse import unquote

from flask import Flask, request

app = Flask(__name__)

@app.route("/read", methods=['POST'])
def read_file():
    path = unquote(request.json['file_path'])

    with open(path, "rb") as _file:
        contents = _file.read()
    
    encoded = b64encode(contents).decode('utf-8')
    return {
        'encoded': encoded
    }

@app.route("/write", methods=['POST'])
def write_file():
    path = unquote(request.json['file_path'])

    decoded_contents = b64decode(request.json['data'])
    with open(path, "wb") as _file:
        _file.write(decoded_contents)

    return {}

if __name__ == '__main__':
    app.run(port=5001)